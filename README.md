## Config

All config files must be in this folder. If there is no option to set this folder
directly, it has to be hardlinked.

* `Produção`: Alteração no arquivo index.php define('ENVIRONMENT', 'development'); para define('ENVIRONMENT', 'production');
* `Instalação em subdiretório`: http://dominio.com/folder_project/install
* `Instalação`: Seguir as etapas de instalação.


* `OBS`: Arrumar o script para redirecionamento de instalação igual ao do app.rodrigo3d.com





# Página em Branco
Esta é uma página simples para começar a construir o seu site como um Lego!

# Demo
[Demo online](https://rodrigo3d.com/page-blank/)

![Color Theme](http://quinalha.me/jekyll-help-center-theme/assets/img/readme/responsive.png)

# Color theme
edit in _config.yml
```
color_theme:  "#0081ff"
color_text:  "#fff"
```

# Instalação
```
git clone https://bitbucket.org/rodrigo3d/advogados
```
https:https://bitbucket.org/rodrigo3d/advogados

# Configurações
Iniciando aplicação em produção.
```
index.php

linha 21: define('ENVIRONMENT', 'production');
```
Iniciando aplicação no idioma Português.
```
application/third_party/MX/Controller.php

linha 55: $this->lang->load('admin', 'portuguese');
```

## License
The theme is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).







